﻿namespace Headless.Extensions
{
    using Newtonsoft.Json;

    public static class JsonExtensions
    {
        #region Public Methods and Operators

        public static string ToJson(this object o)
        {
            return JsonConvert.SerializeObject(o);
        }

        #endregion
    }
}