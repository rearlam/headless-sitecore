﻿namespace Headless.Extensions
{
    using System.IO;
    using System.Web;
    using System.Web.Mvc;

    using Sitecore.Diagnostics;
    using Sitecore.Mvc.Common;
    using Sitecore.Mvc.Helpers;
    using Sitecore.Mvc.Pipelines;
    using Sitecore.Mvc.Pipelines.Response.RenderPlaceholder;

    public static class ExpereinceEditorExtensions
    {
        #region Public Methods and Operators

        public static HtmlString JsonArrayPlaceholder(this SitecoreHelper sitecoreHelper, string placeholderName)
        {
            var returnValue =
                new HtmlString(
                    string.Format("\"{0}\" : [ {1} ]", placeholderName, Placeholder(sitecoreHelper, placeholderName)));
            return returnValue;
        }

        public static HtmlString JsonPropertyPlaceholder(this SitecoreHelper sitecoreHelper, string placeholderName)
        {
            var returnValue =
                new HtmlString(
                    string.Format("\"{0}\" : {1}", placeholderName, Placeholder(sitecoreHelper, placeholderName)));

            return returnValue;
        }

        // reflected out of Sitecore.Mvc.Helpers.SitecoreHelper
        private static HtmlString Placeholder(SitecoreHelper sitecoreHelper, string placeholderName)
        {
            Assert.ArgumentNotNull((object)placeholderName, "placeholderName");
            using (ContextService.Get().Push<ViewContext>(sitecoreHelper.ViewContext))
            {
                StringWriter stringWriter = new StringWriter();
                PipelineService.Get().RunPipeline<RenderPlaceholderArgs>("mvc.renderPlaceholder", new RenderPlaceholderArgs(placeholderName, (TextWriter)stringWriter, this.CurrentRendering));
                return new HtmlString(stringWriter.ToString());
            }
        }

        #endregion
    }
}