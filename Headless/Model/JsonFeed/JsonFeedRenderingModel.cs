﻿namespace Headless.Model.JsonFeed
{
    using Newtonsoft.Json;

    using Sitecore.Data.Items;
    using Sitecore.Mvc.Presentation;

    public abstract class JsonFeedRenderingModel : RenderingModel
    {
        [JsonIgnore]
        public override Item Item
        {
            get
            {
                return base.Item;
            }
        }

        [JsonIgnore]
        public override Item PageItem
        {
            get
            {
                return base.PageItem;
            }
        }

        [JsonIgnore]
        public override Rendering Rendering
        {
            get
            {
                return base.Rendering;
            }

            set
            {
                base.Rendering = value;
            }
        }
    }
}