﻿namespace Headless.Model.JsonFeed
{
    using System.Runtime.Serialization;

    public class BodyContentViewModel : JsonFeedRenderingModel
    {
        #region Public Properties

        [DataMember]
        public string Title
        {
            get
            {
                return this.Item["Title"];
            }
        }

        [DataMember]
        public string Body
        {
            get
            {
                return this.Item["Body"];
            }
        }

        #endregion
    }
}